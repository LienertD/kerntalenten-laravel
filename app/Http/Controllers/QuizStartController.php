<?php

namespace App\Http\Controllers;

use App\Models\Pupil;

class QuizStartController extends Controller
{
    public function index(Pupil $pupil)
    {
        return view('quiz.start.index', ['pupil' => $pupil]);
    }
}
