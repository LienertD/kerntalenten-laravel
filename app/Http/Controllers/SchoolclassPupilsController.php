<?php

namespace App\Http\Controllers;

use App\Models\Schoolclass;
use Illuminate\Http\Request;

class SchoolclassPupilsController extends Controller
{
    public function store(Schoolclass $schoolclass, Request $request)
    {
        $schoolclass->pupils()->create([
            'name' => $request->name,
            'schoolclass_id' => $schoolclass->id,
        ]);

        return back();
    }
}
