<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pupil;
use App\Models\Question;

class PupilAnswersController extends Controller
{
    public function index(Pupil $pupil)
    {
        return view('pupils.answers.index', ['pupil' => $pupil, 'number_of_questions' => Question::count()]);
    }
}
