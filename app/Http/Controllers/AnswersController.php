<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Pupil;
use App\Models\Question;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    public function store(Request $request)
    {
        $pupil_id  = $request->pupil_id;
        $question_id = $request->question_id;

        Answer::updateOrCreate(
            [
                'pupil_id' => $pupil_id,
                'question_id' => $question_id
            ],
            [
                'value' => $request->answervalue,
            ]
        );

        /*$current_question = Question::find($request->question_id);
        if ($current_question != null) {
            $next_question = $current_question->nextQuestion();
            if ($next_question != null) {
                return redirect()->route('quiz.questions', ['pupil' => Pupil::find($pupil_id), 'question' => $next_question]);
            }
        }*/

        return back();
    }
}
