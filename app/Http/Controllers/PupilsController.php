<?php

namespace App\Http\Controllers;

use App\Models\Pupil;
use Illuminate\Http\Request;

class PupilsController extends Controller
{
    public function store(Request $request)
    {
        // Validate
        $this->validate($request, [
            'name' => 'required'
        ]);

        Pupil::create([
            'name' => $request->name,
            'schoolclass_id' => $request->schoolclass_id
        ]);

        return back();
    }

    public function edit(Pupil $pupil)
    {
        return view('pupils.edit', [
            'pupil' => $pupil
        ]);
    }

    public function update(Request $request, Pupil $pupil)
    {
        $pupil->update($request->all());

        return redirect()->route('schoolclasses.show', ['schoolclass' => $pupil->schoolclass]);
    }

    public function destroy(Pupil $pupil)
    {
        $pupil->delete();

        return back();
    }
}
