<?php

namespace App\Http\Controllers;

use App\Models\Pupil;
use PhpOffice\PhpWord\TemplateProcessor;

class PupilsReportController extends Controller
{
    public function index(Pupil $pupil)
    {
        $orderedAnswers  = $pupil->answers()->orderBy('value', 'desc')->get();
        $answersAboveThreshold =  $orderedAnswers->where('value', '>=', 0.7)->take(10);

        if ($answersAboveThreshold->count() >= 5) {
            $answersOnReport = $answersAboveThreshold;
        } else {
            $answersOnReport = $orderedAnswers->take(5);
        }

        $templateProcessor = new TemplateProcessor('word_templates/report-template.docx');
        $templateProcessor->setValue('pupil_name', $pupil->name);
        $templateProcessor->setValue('schoolclass_name', $pupil->schoolclass->name);
        $templateProcessor->cloneBlock('answer_result_block', $answersOnReport->count(), true, true);

        for ($i = 0; $i < $answersOnReport->count(); $i++) {
            $templateProcessor->setValue('answer_result#' . ($i + 1), str_replace("\n", "<w:br/>",  $orderedAnswers[$i]->question->result));
        }

        $fileName = 'kerntalenten verslag ' . $pupil->name . '.docx';
        $templateProcessor->saveAs($fileName);
        return response()->download($fileName)->deleteFileAfterSend(true);
    }
}
