<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pupil;

class QuizEndController extends Controller
{
    public function index(Pupil $pupil)
    {
        return view('quiz.end.index', ['pupil' => $pupil]);
    }
}
