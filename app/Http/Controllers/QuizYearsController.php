<?php

namespace App\Http\Controllers;

use App\Models\Year;


class QuizYearsController extends Controller
{
    public function index()
    {
        $years = Year::get();
        return view('quiz.years.index', ['years' => $years]);
    }
}
