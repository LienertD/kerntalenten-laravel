<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;

class QuestionsController extends Controller
{
    public function index()
    {
        $questions = Question::get();

        return view('questions.index', [
            'questions' => $questions
        ]);
    }

    public function store(Request $request)
    {
        // Validate
        $this->validate($request, [
            'text' => 'required',
            'result' => 'required',
        ]);

        // Create post in db
        Question::create($request->only('text', 'result'));

        return back();
    }

    public function destroy(Question $question)
    {
        $question->delete();

        return back();
    }

    public function edit(Question $question)
    {
        return view('questions.edit', [
            'question' => $question
        ]);
    }

    public function update(Request $request, Question $question)
    {
        $question->update($request->all());

        return redirect()->route('questions');
    }
}
