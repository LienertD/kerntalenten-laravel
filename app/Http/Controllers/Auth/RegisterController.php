<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function index()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        // validate
        // validate() throws exception when not succesfull, returns to page with an 'error bag'
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        // store user
        User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        // sign in user
        auth()->attempt($request->only('email', 'password'));

        // redirect
        return redirect()->route('home');
    }
}
