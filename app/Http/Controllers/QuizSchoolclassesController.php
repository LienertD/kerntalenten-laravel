<?php

namespace App\Http\Controllers;

use App\Models\Schoolclass;
use App\Models\Year;

class QuizSchoolclassesController extends Controller
{
    public function index(Year $year)
    {
        $schoolclasses =  Schoolclass::where('year_id', $year->id)->get();;
        return view('quiz.schoolclasses.index', ['schoolclasses' => $schoolclasses]);
    }
}
