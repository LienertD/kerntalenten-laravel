<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schoolclass;
use App\Models\Year;

class SchoolclassController extends Controller
{
    public function index()
    {
        $years = Year::orderBy('created_at', 'desc')->get();

        return view('schoolclasses.index', [
            'years' => $years,
        ]);
    }

    public function store(Request $request)
    {
        // Validate
        $this->validate($request, [
            'name' => 'required',
        ]);

        // Store year in db
        Schoolclass::create([
            'name' => $request->name,
            'year_id' => $request->year_id
        ]);

        return back();
    }

    public function show(Schoolclass $schoolclass)
    {
        return view('schoolclasses.show', [
            'schoolclass' => $schoolclass
        ]);
    }

    public function destroy(Schoolclass $schoolclass)
    {
        $schoolclass->delete();

        return back();
    }

    public function edit(Schoolclass $schoolclass)
    {
        return view('schoolclasses.edit', [
            'schoolclass' => $schoolclass
        ]);
    }

    public function update(Request $request, Schoolclass $schoolclass)
    {
        $schoolclass->update($request->all());

        return redirect()->route('schoolclasses');
    }
}
