<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Pupil;
use App\Models\Question;

class QuizQuestionsController extends Controller
{
    public function index(Pupil $pupil, Question $question)
    {
        $questions = Question::get();

        $givenanswer = Answer::firstWhere([['pupil_id', $pupil->id], ['question_id', $question->id]]);
        $given_answer_value = -1;
        if ($givenanswer != null) {
            $given_answer_value =  $givenanswer->value;
        }

        return view('quiz.questions.index', [
            'pupil' => $pupil,
            'question' => $question,
            'answered_value' => $given_answer_value,
            'questionIndex' => $questions->search($question),
            'numberOfQuestions' => $questions->count(),
        ]);
    }
}
