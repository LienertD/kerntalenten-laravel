<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Year;

class YearController extends Controller
{
    public function index()
    {
        $years = Year::orderBy('created_at', 'desc')->get();

        return view('years.index', [
            'years' => $years,
        ]);
    }

    public function store(Request $request)
    {
        // Validate
        $this->validate($request, [
            'name' => 'required'
        ]);

        // Store year in db
        Year::create([
            'name' => $request->name,
        ]);

        return back();
    }

    public function destroy(Year $year)
    {
        $year->delete();

        return back();
    }

    public function edit(Year $year)
    {
        return view('years.edit', [
            'year' => $year
        ]);
    }

    public function update(Request $request, Year $year)
    {
        //dd($request);

        $year->update($request->all());

        return redirect()->route('years');
    }
}
