<?php

namespace App\Http\Controllers;

use App\Models\Pupil;
use App\Models\Schoolclass;

class QuizPupilsController extends Controller
{
    public function index(Schoolclass $schoolclass)
    {
        $pupils =  Pupil::where('schoolclass_id', $schoolclass->id)->get();;
        return view('quiz.pupils.index', ['pupils' => $pupils]);
    }
}
