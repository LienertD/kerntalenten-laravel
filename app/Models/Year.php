<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function schoolclasses()
    {
        return $this->hasMany(Schoolclass::class);
    }
}
