<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Year;

class Schoolclass extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'year_id'
    ];

    public function pupils()
    {
        return $this->hasMany(Pupil::class);
    }

    public function year()
    {
        return $this->belongsTo(Year::class);
    }
}
