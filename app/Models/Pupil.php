<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Answer;

class Pupil extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'schoolclass_id'
    ];

    public function schoolclass()
    {
        return $this->belongsTo(Schoolclass::class);
    }

    public function getFirstUnansweredQuestion()
    {
        return Question::first();
    }

    public function hasAnsweredAllQuestions()
    {
        $questions = Question::get();
        foreach ($questions as $question) {
            if (!Answer::where([
                'pupil_id' => $this->id,
                'question_id' => $question->id
            ])->exists()) {
                return false;
            }
        }

        return true;
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
