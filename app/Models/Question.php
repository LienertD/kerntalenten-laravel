<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'text',
        'result'
    ];

    public function nextQuestion()
    {
        $questions = Question::get();
        $currentIndex = $questions->search($this);
        if ($currentIndex + 1 <= Question::count() - 1) {
            return $questions->values()->get($currentIndex + 1);
        }

        return null;
    }

    public function previousQuestion()
    {
        $questions = Question::get();
        $currentIndex = $questions->search($this);
        if ($currentIndex - 1 >= 0) {
            return $questions->values()->get($currentIndex - 1);
        }

        return null;
    }
}
