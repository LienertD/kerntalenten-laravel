<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Question;
use App\Models\Pupil;

class Answer extends Model
{
    use HasFactory;

    protected $fillable = [
        'pupil_id',
        'question_id',
        'value'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function pupil()
    {
        return $this->belongsTo(Pupil::class);
    }
}
