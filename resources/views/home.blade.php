@extends('layouts.app')

@section('content')
<div class="flex justify-center">

    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Kerntalenten</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">
            <a href="{{ route('quiz.years') }}" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Start vragen</a>
        </div>
    </div>

</div>
@endsection
