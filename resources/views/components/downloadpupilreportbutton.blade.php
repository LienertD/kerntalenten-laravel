@props(['pupil' => $pupil])

<a href="{{ route('pupils.report', ['pupil' => $pupil]) }}" class="bg-blue-500 text-white py-1 px-4 rounded inline-flex items-center mb-4">
    <svg class="w-4 h-4 mr-2 fill-current text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
        <path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z" />
    </svg>
    <span>Download verslag</span>
</a>
