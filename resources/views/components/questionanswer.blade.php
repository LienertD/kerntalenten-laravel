@props(['answer' => $answer])

<p class="whitespace-pre-wrap pb-4"><b>Vraag:</b> {{ $answer->question->text }}</p>
<p><b>Hoe graag doe jij dit:</b> {{ ($answer->value * 100) }}% <a class="text-blue-500" href="{{ route('quiz.questions', ['pupil'=> $answer->pupil, 'question' => $answer->question]) }}">antwoord aanpassen</a></p>
