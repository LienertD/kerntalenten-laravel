@extends('layouts.app')

@section('content')
<div class="flex justify-center">
    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Schooljaren</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">

            @if($years->count())

            @foreach($years as $year)

            <div class="mb-2">
                <p>{{ $year->name }}</p>

                @auth
                <a href="{{ route('years.edit', $year) }}" class="text-blue-500">Naam wijzigen</a>

                <form action="{{ route('years.destroy', $year) }}" method="POST">
                    @csrf
                    @method('DELETE')

                    <button type="submit" class="text-blue-500">Verwijderen</button>
                </form>
                @endauth

            </div>

            @endforeach

            @else

            <p>Nog geen schooljaren.</p>

            @endif

            @auth
            <div class="w-full ">
                <form action="{{ route('years') }}" method="POST" class="mb-4">

                    @csrf

                    <div class="mb-4">

                        <input type="text" name="name" id="name" class="bg-gray-100 border-2 p-2 rounded-lg @error('name') border-red-500 @enderror" placeholder="Schooljaar naam" />

                        @error('name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                        @enderror

                        <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Jaar toevoegen</button>
                    </div>

                </form>
            </div>
            @endauth

        </div>
    </div>
</div>
@endsection
