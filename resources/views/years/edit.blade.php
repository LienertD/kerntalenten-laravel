@extends('layouts.app')

@section('content')
<div class="flex justify-center">
    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Schooljaar '{{ $year->name }}' bewerken</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">

            @auth
            <div class="w-full ">
                <form method="POST" class="mb-4">
                    @csrf
                    @method('PUT')

                    <div class="mb-4">

                        <input type="text" name="name" id="name" class="bg-gray-100 border-2 p-2 rounded-lg @error('name') border-red-500 @enderror" placeholder="Schooljaar naam" value="{{ $year->name }}" />

                        @error('name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                        @enderror

                        <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Aanpassen</button>
                    </div>

                </form>
            </div>
            @endauth

        </div>
    </div>
</div>
@endsection
