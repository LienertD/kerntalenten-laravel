@extends('layouts.app')

@section('content')
<div class="flex justify-center">

    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Vraag {{ ($questionIndex + 1) }}/{{ $numberOfQuestions }}</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">
            <p class="pb-4 whitespace-pre-wrap">{{ $question->text }}</p>

            <p class="pb-2"><b>Hoe graag doe jij dit?</b></p>

            <div class="pb-2 border-l border-r border-b border-blue-500">
                <ul class="w-full mb-1 table table-fixed border-collapse">
                    <li class="text-center bg-blue-500 text-white table-cell border-r border-white p-1">hekel</li>
                    <li class="text-center bg-blue-500 text-white table-cell border-r border-white p-1">niet zo graag</li>
                    <li class="text-center bg-blue-500 text-white table-cell border-r border-white p-1">een beetje graag</li>
                    <li class="text-center bg-blue-500 text-white table-cell border-r border-white p-1">graag</li>
                    <li class="text-center bg-blue-500 text-white table-cell p-1">super graag</li>
                </ul>
                <table class="w-full">
                    <tr>
                        @php
                            $numberOfOptions = 21;
                        @endphp

                        @for($i=0; $i <= $numberOfOptions; $i++)

                            @php
                                $answerValue = $i / $numberOfOptions;
                                $currentAnswerIsSelected = false;
                                if($answered_value >= 0)
                                {
                                    $minAnswerValue = $answerValue - ((1 / $numberOfOptions) / 2);
                                    $maxAnswerValue = $answerValue + ((1 / $numberOfOptions) / 2);
                                    if($minAnswerValue <= $answered_value && $answered_value <= $maxAnswerValue)
                                    {
                                        $currentAnswerIsSelected = true;
                                    }
                                }
                            @endphp

                            @if($currentAnswerIsSelected)
                                <td><button class="bg-blue-500 text-white py-2 w-full rounded font-medium">○</button></td>
                            @else
                                <td>
                                    <form action="{{ route('answers') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="answervalue" value="{{ $answerValue }}">
                                        <input type="hidden" name="pupil_id" value="{{ $pupil->id }}">
                                        <input type="hidden" name="question_id" value="{{ $question->id }}">
                                        <button type="submit" class="text-blue-500 py-2 w-full rounded font-medium">○</button>
                                    </form>
                                </td>
                            @endif

                        @endfor
                    </tr>
                </table>
            </div>

            <div class="pt-4 w-full inline-block">
                @if($questionIndex > 0)
                    <a href="{{ route('quiz.questions', ['pupil' => $pupil, 'question' => $question->previousQuestion() ]) }}" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Vorige vraag</a>
                @endif

                @if($pupil->hasAnsweredAllQuestions())
                    <a href="{{ route('quiz.end', ['pupil'=>$pupil]) }}" class="bg-gray-500 text-white ml-2 px-4 py-2 rounded font-medium float-right">Alle vragen ingevuld, beëindig</a>
                @endif

                @if($questionIndex < $numberOfQuestions - 1)
                <a href="{{ route('quiz.questions', ['pupil' => $pupil, 'question' => $question->nextQuestion() ]) }}" class="bg-blue-500 text-white px-4 py-2 rounded font-medium float-right">Volgende vraag</a>
                @endif
            </div>

        </div>
    </div>

</div>
@endsection
