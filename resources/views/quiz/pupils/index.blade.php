@extends('layouts.app')

@section('content')
<div class="flex justify-center">

    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Vragen</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">
            <h1 class="text-2xl font-medium mb-4">Selecteer jouw naam:</h1>

            <ul>
                @foreach($pupils as $pupil)
                <li class="pb-4"><a href="{{ route('quiz.start', ['pupil' => $pupil]) }}" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">{{ $pupil->name }}</a></li>
                @endforeach
            </ul>

        </div>
    </div>

</div>
@endsection
