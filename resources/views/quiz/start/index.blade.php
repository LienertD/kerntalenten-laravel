@extends('layouts.app')

@section('content')
<div class="flex justify-center">

    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Vragen</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">
            <h1 class="text-2xl font-medium mb-4">Dag {{ $pupil->name }} uit {{ $pupil->schoolclass->name }} ({{ $pupil->schoolclass->year->name }})!</h1>
            <p>Niet juist? Klik <a class="text-blue-500" href="{{ route('quiz.years') }}">hier</a> om opnieuw te beginnen.</p>
            <br>

            <a href="{{ route('quiz.questions', ['pupil' => $pupil, 'question' => $pupil->getFirstUnansweredQuestion() ]) }}" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Vragen starten</a>
        </div>
    </div>

</div>
@endsection
