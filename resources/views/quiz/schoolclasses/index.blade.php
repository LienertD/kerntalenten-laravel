@extends('layouts.app')

@section('content')
<div class="flex justify-center">

    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Vragen</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">
            <h1 class="text-2xl font-medium mb-4">Selecteer klas:</h1>

            <div>
                @foreach($schoolclasses as $schoolclass)
                <a href="{{ route('quiz.pupils', ['schoolclass' => $schoolclass]) }}" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">{{ $schoolclass->name }}</a>
                @endforeach
            </div>

        </div>
    </div>

</div>
@endsection
