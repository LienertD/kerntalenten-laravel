@extends('layouts.app')

@section('content')
<div class="flex justify-center">

    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Vragen</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">
            <h1 class="text-2xl font-medium mb-4">Overzicht antwoorden {{ $pupil->name }} uit {{ $pupil->schoolclass->name }} ({{ $pupil->schoolclass->year->name }})</h1>

            <ul class="list-decimal">
                @foreach($pupil->answers as $answer)
                <li class="pb-4">
                    <x-questionanswer :answer="$answer" />
                </li>
                @endforeach
            </ul>

            <a href="{{ route('home') }}" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Klaar</a>
        </div>
    </div>

</div>
@endsection
