@extends('layouts.app')

@section('content')
<div class="flex justify-center">
    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Vragen</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">

            @if($questions->count())

            <ul class="list-decimal">

                @foreach($questions as $question)
                <li class="mb-4">
                    <p class="whitespace-pre-wrap pb-2"><b>Vraag: </b>{{ $question->text }}</p>
                    <p class="whitespace-pre-wrap pb-2"><b>Resultaat: </b>{{ $question->result }}</p>
                    <a href="{{ route('questions.edit', ['question'=>$question]) }}" class="text-blue-500">bewerken</a>
                    <form action="{{ route('questions.destroy', ['question'=>$question]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="text-blue-500" type="submit">verwijderen</button>
                    </form>
                </li>
                @endforeach

            </ul>

            @else

            <p>Nog geen vragen.</p>

            @endif

            @auth
            <div class="w-full mt-4">
                <form action="{{ route('questions') }}" method="POST" class="mb-4">
                    @csrf

                    <div class="mb-4">

                        <label for="text" class="sr-only">Vraag</label>
                        <textarea name="text" id="text" cols="30" rows="4" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('text') border-red-500 @enderror" placeholder="Vraag"></textarea>

                        @error('text')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                        @enderror

                        <label for="result" class="sr-only">Resultaat</label>
                        <textarea name="result" id="result" cols="30" rows="4" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('result') border-red-500 @enderror" placeholder="Resultaat bij hoge score"></textarea>

                        @error('result')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                        @enderror

                        <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Vraag toevoegen</button>
                    </div>

                </form>
            </div>
            @endauth

        </div>
    </div>
</div>
@endsection
