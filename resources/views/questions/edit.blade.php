@extends('layouts.app')

@section('content')
<div class="flex justify-center">
    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Vraag bewerken</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">

            @auth
            <div class="w-full ">
                <form method="POST" class="mb-4">
                    @csrf
                    @method('PUT')

                    <div class="mb-4">

                        <label for="text" class="sr-only">Vraag</label>
                        <textarea name="text" id="text" cols="30" rows="4" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('text') border-red-500 @enderror" placeholder="Vraag">{{ $question->text }}</textarea>

                        @error('text')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                        @enderror

                        <label for="result" class="sr-only">Resultaat</label>
                        <textarea name="result" id="result" cols="30" rows="4" class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('result') border-red-500 @enderror" placeholder="Resultaat bij hoge score">{{ $question->result }}</textarea>

                        @error('result')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                        @enderror

                        <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Aanpassingen opslaan</button>
                    </div>

                </form>
            </div>
            @endauth

        </div>
    </div>
</div>
@endsection
