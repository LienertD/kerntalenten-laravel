@extends('layouts.app')

@section('content')
<div class="flex justify-center">
    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">User {{ $user->name }} posts</h1>
            <p> Posted {{ $posts->count() }} posts and received {{ $user->receivedLikes->count() }} likes.</p>
        </div>

        <div class="bg-white p-6 rounded-lg">

            @if($posts->count())

            @foreach($posts as $post)

            <x-post :post="$post" :linkdetails="true" />

            @endforeach

            {{ $posts->links() }}

            @else

            <p>No posts.</p>

            @endif

        </div>
    </div>
</div>
@endsection
