<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Kerntalenten</title>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>

    <body class="bg-gray-200">

        <nav class="bg-white flex justify-between mb-6">

            <ul class="flex items-center">
                <li class="pl-6 pr-3">
                    <img src="{{ asset('images/logo.png') }}" style="max-height: 72px" alt="">
                </li>

                <li class="px-3 py-6">
                    <a href="{{ route('home') }}">Home</a>
                </li>

                @auth
                <li class="px-3 py-6">
                    <a href="{{ route('years') }}">Schooljaren</a>
                </li>
                <li class="px-3 py-6">
                    <a href="{{ route('schoolclasses') }}">Klassen</a>
                </li>
                <li class="px-3 py-6">
                    <a href="{{ route('questions') }}">Vragen</a>
                </li>
                @endauth
            </ul>

            <ul class="flex items-center">

                @auth
                <li class="px-3 py-6">Ingelogd als {{ auth()->user()->email }}</li>

                <li class="px-3 py-6 pr-6">
                    <form action="{{ route('logout') }}" method="POST" class="inline">
                        @csrf
                        <button type="submit">Uitloggen</button>
                    </form>
                </li>
                @endauth

                @guest
                <li class="px-3 py-6 pr-6">
                    <a href="{{ route('login') }}">Login voor leerkrachten</a>
                </li>
                @endguest

            </ul>

        </nav>

        @yield('content')

    </body>

</html>
