@extends('layouts.app')

@section('content')
<div class="flex justify-center">

    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Antwoorden {{ $pupil->name }} uit {{ $pupil->schoolclass->name }} ({{ $pupil->schoolclass->year->name }})</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">
            <p class="pb-4">{{ $pupil->answers->count() }} van de {{ $number_of_questions }} vragen beantwoord.</p>

            @if($pupil->answers->count())

            <x-downloadpupilreportbutton :pupil="$pupil" />

            <ul class="list-decimal">
                @foreach($pupil->answers as $answer)
                <li class="pb-4">
                    <x-questionanswer :answer="$answer" />
                </li>
                @endforeach
            </ul>
            @else
            <p>Er zijn nog geen antwoorden.</p>
            @endif
        </div>
    </div>

</div>
@endsection
