@extends('layouts.app')

@section('content')
<div class="flex justify-center">
    <div class="w-8/12">

        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Klas {{ $schoolclass->name }}</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">

            @if($schoolclass->pupils->count())

            <ul class="pb-4 list-decimal">
                @foreach ($schoolclass->pupils as $pupil)
                <li>{{ $pupil->name }}
                    <br>
                    <a href="{{ route('pupils.edit', ['pupil'=>$pupil]) }}" class="text-blue-500">naam wijzigen</a>
                    <br>
                    <form action="{{ route('pupils.destroy', ['pupil'=>$pupil]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="text-blue-500">verwijderen</button></form>
                    <a href="{{ route('pupils.answers', ['pupil' => $pupil]) }}" class="text-blue-500">bekijk antwoorden</a>
                    <br>
                    @if($pupil->answers->count())
                    <x-downloadpupilreportbutton :pupil="$pupil" />
                    @endif
                </li>
                @endforeach
            </ul>

            @else
            <p>Nog geen leerlingen.</p>
            @endif

            <!-- Add pupil -->
            @auth
            <div class="w-full ">
                <form action="{{ route('schoolclasses.pupils', ['schoolclass'=>$schoolclass]) }}" method="POST" class="mb-4">
                    @csrf

                    <div class="mb-4">

                        <input type="text" name="name" id="name" class="bg-gray-100 border-2 p-2 rounded-lg @error('name') border-red-500 @enderror" placeholder="Naam leerling" />

                        @error('name')
                        <div class="text-red-500 mt-2 text-sm">
                            {{ $message }}
                        </div>
                        @enderror

                        <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Leerling toevoegen</button>
                    </div>

                </form>
            </div>
            @endauth

        </div>

    </div>
</div>
@endsection
