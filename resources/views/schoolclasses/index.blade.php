@extends('layouts.app')

@section('content')
<div class="flex justify-center">
    <div class="w-8/12">
        <div class="p-6">
            <h1 class="text-2xl font-medium mb-1">Klassen</h1>
        </div>

        <div class="bg-white p-6 rounded-lg">

            @if($years->count())

            @foreach($years as $year)

            <div class="mb-4">
                <p class="text-2xl font-medium">{{ $year->name }}</p>

                @if($year->schoolclasses ->count())

                @foreach($year->schoolclasses as $schoolclass)

                <div class="mb-2">
                    <p>{{ $schoolclass->name }}</p>

                    @auth
                    <a href="{{ route('schoolclasses.show', $schoolclass) }}" class="text-blue-500">leerlingen</a>
                    <br>
                    <a href="{{ route('schoolclasses.edit', $schoolclass) }}" class="text-blue-500">naam wijzigen</a>
                    <form action="{{ route('schoolclasses.destroy', $schoolclass) }}" method="POST">
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="text-blue-500">verwijderen</button>
                    </form>
                    @endauth

                </div>

                @endforeach

                @else

                <p class="mb-2">Geen klassen.</p>

                @endif

            </div>

            @endforeach

            @else

            @endif

            @auth
            <div class="w-full pt-4">
                <form action="{{ route('schoolclasses') }}" method="POST">

                    @csrf

                    <input type="text" name="name" id="name" class="bg-gray-100 border-2 p-2 rounded-lg @error('name') border-red-500 @enderror" placeholder="6A" />

                    @error('name')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                    @enderror

                    <select name="year_id" class="p-2 border-2">

                        @foreach ($years as $year)

                        <option value="{{ $year->id }}">

                            {{ $year->name }}

                        </option>

                        @endforeach

                    </select>

                    <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded font-medium">Klas toevoegen</button>

                </form>
            </div>
            @endauth

        </div>
    </div>
</div>
@endsection
