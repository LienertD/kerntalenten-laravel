<?php

use App\Http\Controllers\AnswersController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\PupilAnswersController;
use App\Http\Controllers\PupilsController;
use App\Http\Controllers\PupilsReportController;
use App\Http\Controllers\QuestionsController;
use App\Http\Controllers\QuizEndController;
use App\Http\Controllers\QuizPupilsController;
use App\Http\Controllers\QuizQuestionsController;
use App\Http\Controllers\QuizSchoolclassesController;
use App\Http\Controllers\QuizStartController;
use App\Http\Controllers\QuizYearsController;
use App\Http\Controllers\SchoolclassController;
use App\Http\Controllers\SchoolclassPupilsController;
use App\Http\Controllers\YearController;

Route::get('/', function () {
    return view('home');
});

Route::get('home', function () {
    return view('home');
})->name('home');

Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/login', 'App\Http\Controllers\Auth\LoginController@index')->name('login');
Route::post('/login', 'App\Http\Controllers\Auth\LoginController@store');

Route::post('/logout', 'App\Http\Controllers\Auth\LogoutController@store')->name('logout');

Route::get('/years', [YearController::class, 'index'])->name('years');
Route::post('/years', [YearController::class, 'store']);
Route::delete('/years/{year}', [YearController::class, 'destroy'])->name('years.destroy');
Route::get('/years/{year}/edit', [YearController::class, 'edit'])->name('years.edit');
Route::put('/years/{year}/edit', [YearController::class, 'update'])->name('years.edit');

Route::get('/schoolclasses', [SchoolclassController::class, 'index'])->name('schoolclasses');
Route::post('/schoolclasses', [SchoolclassController::class, 'store']);
Route::delete('/schoolclasses/{schoolclass}', [SchoolclassController::class, 'destroy'])->name('schoolclasses.destroy');
Route::get('/schoolclasses/{schoolclass}/edit', [SchoolclassController::class, 'edit'])->name('schoolclasses.edit');
Route::put('/schoolclasses/{schoolclass}/edit', [SchoolclassController::class, 'update'])->name('schoolclasses.edit');
Route::get('/schoolclasses/{schoolclass}', [SchoolclassController::class, 'show'])->name('schoolclasses.show');

Route::post('/schoolclasses/{schoolclass}/pupils', [SchoolclassPupilsController::class, 'store'])->name('schoolclasses.pupils');
Route::get('/pupils/{pupil}/edit', [PupilsController::class, 'edit'])->name('pupils.edit');
Route::put('/pupils/{pupil}/edit', [PupilsController::class, 'update'])->name('pupils.edit');
Route::delete('/pupils/{pupil}', [PupilsController::class, 'destroy'])->name('pupils.destroy');
Route::get('/pupils/{pupil}/answers', [PupilAnswersController::class, 'index'])->name('pupils.answers');
Route::get('/pupils/{pupil}/report', [PupilsReportController::class, 'index'])->name('pupils.report');

Route::get('/questions', [QuestionsController::class, 'index'])->name('questions');
Route::post('/questions', [QuestionsController::class, 'store']);
Route::delete('/questions/{question}', [QuestionsController::class, 'destroy'])->name('questions.destroy');
Route::get('/questions/{question}/edit', [QuestionsController::class, 'edit'])->name('questions.edit');
Route::put('/questions/{question}/edit', [QuestionsController::class, 'update'])->name('questions.edit');

Route::get('/quiz/years', [QuizYearsController::class, 'index'])->name('quiz.years');
Route::get('/quiz/{year}/schoolclasses', [QuizSchoolclassesController::class, 'index'])->name('quiz.schoolclasses');
Route::get('/quiz/{schoolclass}/pupils', [QuizPupilsController::class, 'index'])->name('quiz.pupils');
Route::get('/quiz/{pupil}/start', [QuizStartController::class, 'index'])->name('quiz.start');
Route::get('/quiz/{pupil}/start', [QuizStartController::class, 'index'])->name('quiz.start');
Route::get('/quiz/{pupil}/questions/{question}', [QuizQuestionsController::class, 'index'])->name('quiz.questions');
Route::get('/quiz/{pupil}/end', [QuizEndController::class, 'index'])->name('quiz.end');

Route::post('/answers', [AnswersController::class, 'store'])->name('answers');
